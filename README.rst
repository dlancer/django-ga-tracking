==================
django-ga-tracking
==================

django-ga-tracking is a simple Django pluggable app that provides a context
processor and template for adding `Google Analytics`_ tracking code to your
pages.

Alternatives and differences
============================

There are alternatives like `django-google-analytics`_ and `Django Analytics
Middleware`_, but this project begun as both solutions seem to introduce
unnecessary complexity and/or lack the ability to configure analytics via
Django settings module. The latter factor was particularly important for this
project because hard-coding configuration or storing configuration in a
databaes column was something we absolutely wanted to avoid.

Installation
============

Install using pip::

    pip install django-ga-tracking

Basic usage
===========

Add ``ga_tracking`` to ``INSTALLED_APPS``.

Next, add the ``ga_tracking_id`` context processor to
``TEMPLATE_CONTEXT_PROCESSORS``::

    TEMPLATE_CONTEXT_PROCESSORS = (
        ....
        'ga_tracking.context_processors.ga_tracking_id',
    )

Define a ``GA_TRACKING_ID`` setting and assign your tracking ID to it.  For
example::

    GA_TRACKING_ID = 'UA-12345678-9'

Finally, add the tracker code template somewhere in your own template::

    {% include "ga_tracking/ga.html" %}

Tracking superusers
===================

By default, django-ga-analytics will track superusers. You can disable this by
using the ``GA_TRACK_SUPERUSER`` setting. Setting it to ``False`` will cause
the tracking code to not be renedered for any user that has ``is_superuser``
property set to ``True``.

New tracking code
=================

Google Analytics offers an alternative tracking code (``analytics.js``). This
code is also available in django-ga-tracking. Since the tracking code uses both
the property ID and the top level domain, you will need to specify both. So,
add this to your settings::

    GA_TRACKING_ID = 'UA-12345678-9'
    GA_DOMAIN = 'example.com'

Next, include the alternative template::

    {% include "ga_tracking/ga_new.html" %}

Customization
=============

Because django-ga-tracking uses a simple template to add the code, you can
customize it simply by overriding the template with your own. The ``ga.html``
template contains the default ``<script>`` block as provided by Google. 

The script block is *not* rendered at all if the ``GA_TRACKING_ID`` setting is
not present, so you can, for example, set the setting only in production to
avoid development sites from being tracked, or use different tracking codes on
different deployments. For example::

    # in settings.py
    import os

    GA_TRACKING_ID = os.environ.get('GA_TRACKING_ID')

Customizing the tracking call
-----------------------------

Tracking call itself can be customized. The default tacking calls are::

    // For old API
    _gaq.push(['_trackPageview']);

    // For new API
    ga('send', 'pageview');

These calls are separated out into their own templates:
``ga_tracking/ga_track.js`` and ``ga_tracking/ga_new_track.js`` respectively.
By overriding these templates, you can customize the calls. For more
information on respective APIs, check out the Google Developers documentation
for `ga.js`_ and `analytics.js`_. 

It should be reiterated that the two partial templates are standard Django
templates, so they have full access to template context should you need to
access any of the context variables for the purpose of customization.

Reporting bugs
==============

Please report bugs to Bitbucket `issue tracker`_.

.. _Google Analytics: http://google.com/analytics/
.. _django-google-analytics: http://code.google.com/p/django-google-analytics/
.. _Django Analytics Middleware: http://lethain.com/a-django-middleware-for-google-analytics-repost/
.. _issue tracker: https://bitbucket.org/monwara/django-ga-tracking/issues
.. _ga.js: https://developers.google.com/analytics/devguides/collection/gajs/
.. _analytics.js: https://developers.google.com/analytics/devguides/collection/analyticsjs/
