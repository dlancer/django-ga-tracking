from . import settings

def ga_tracking_id(request):
    try:
        is_super = request.user.is_authenticated() and request.user.is_superuser
    except AttributeError:
        is_super = False
    return dict(
        GA_TRACKING_ID=settings.GA_TRACKING_ID,
        GA_DOMAIN=settings.GA_DOMAIN,
        GA_USER_UNTRACKABLE=(not settings.GA_TRACK_SUPERUSER) and is_super
    )
